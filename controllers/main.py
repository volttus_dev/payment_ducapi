# -*- coding: utf-8 -*-

import json
import logging
import pprint
import werkzeug

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class DUCApiController(http.Controller):
    @http.route([
        '/payment/ducapi/test/',
    ],  type='http', auth='public', methods=['POST'], csrf=False)
    def ducapi_test(self, **post):
        _logger.info('test')
        return "<h1>This is a test</h1>"


    _return_url = '/payment/ducapi/return/'
    @http.route([
        '/payment/ducapi/return',
    ], type='json', auth='public', methods=['POST'], csrf=False)
    def ducapi_return(self, **post):
        data = json.loads(request.httprequest.data)
        _logger.info('Beginning DUCApi form_feedback with post data %s', pprint.pformat(data))  # debug

        if data.get('status') == 'confirmed':  # not in ['CANCELLED', 'REJECTED']:
            payload = {
                "reference": data.get("merchantId"),
                "amount": data.get("amount"),
                "currency": data.get("currency"),
                "status": "done",
            }
            request.env['payment.transaction'].sudo().form_feedback(payload, 'ducapi')
        return werkzeug.utils.redirect('/payment/process')

    _accept_url = '/payment/ducapi/feedback'
    @http.route([
        '/payment/ducapi/feedback',
    ], type='http', auth='public', methods=['POST'], csrf=False)
    def ducapi_form_feedback(self, **post):
        _logger.info('Beginning form_feedback with post data %s', pprint.pformat(post))  # debug
        request.env['payment.transaction'].sudo().form_feedback(post, 'ducapi')
        return werkzeug.utils.redirect('/payment/process')





