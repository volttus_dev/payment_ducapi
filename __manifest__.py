# -*- coding: utf-8 -*-

{
    'name': 'DUC Api Payment Acquirer',
    'category': 'Accounting/Payment',
    'summary': 'Payment Acquirer: DUC Api Implementation',
    'version': '1.0',
    'description': """DUCApi Payment Acquirer""",
    'depends': ['payment'],
    'data': [
        'views/payment_views.xml',
        'views/payment_ducapi_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'installable': True,
    'post_init_hook': 'create_missing_journal_for_acquirers',
    'uninstall_hook': 'uninstall_hook',
}
