# coding: utf-8
import requests
import json
import logging
import pprint
from odoo.tools.float_utils import float_compare


from werkzeug import urls

from odoo import api, fields, models, tools, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.addons.payment_ducapi.controllers.main import  DUCApiController
from odoo.tools.pycompat import to_text

_logger = logging.getLogger(__name__)

# https://docs.ducapi.com/developers/development-resources/currency-codes
CURRENCY_CODE_MAPS = {
     "BHD": 3,
     "CVE": 0,
     "DJF": 0,
     "GNF": 0,
     "IDR": 0,
     "JOD": 3,
     "JPY": 0,
     "KMF": 0,
     "KRW": 0,
     "KWD": 3,
     "LYD": 3,
     "OMR": 3,
     "PYG": 0,
     "RWF": 0,
     "TND": 3,
     "UGX": 0,
     "VND": 0,
     "VUV": 0,
     "XAF": 0,
     "XOF": 0,
     "XPF": 0,
 }

class AcquirerDUCApi(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('ducapi', 'DUCApi')])

    ducapi_api_url = fields.Char('API URL', default="https://backend.ducapp.net/api", required_if_provider='ducapi',
                                 groups='base.group_user')
    ducapi_api_key = fields.Char('API Key', default="test.2de91c0c-60c2-5d4c-ad8c-c1c78de99b55", required_if_provider='ducapi', groups='base.group_user')
    ducapi_mercant_name = fields.Char('DUC Merchant name', default="lazaro.aguila@gmail.com", required_if_provider='ducapi', groups='base.group_user')
    ducapi_merchant_password = fields.Char('DUC Merchant password', default="XXXXXX", required_if_provider='ducapi', groups='base.group_user')
    ducapi_merchant_phone = fields.Char('DUC Merchant phone', default="+5353936403",
                                           required_if_provider='ducapi', groups='base.group_user')
    ducapi_merchant_wallet_address = fields.Char('DUC Wallet address', default="0x065Cc26Ba957334CAd2b68B9FBbAe3843E27f75f", required_if_provider='ducapi', groups='base.group_user')

    ducapi_payment_page = fields.Char('DUC Payment page Url', default='/private/payments/order', required_if_provider='ducapi',
                                      groups='base.group_user')
    ducapi_token = fields.Char('DUC Token', groups='base.group_user', required=False)
    ducapi_qrcode = fields.Text('QRCode', groups='base.group_user', required=False)

    @api.model
    def _ducapi_convert_amount(self, amount, currency):
        """
        DUCApi requires the amount to be multiplied by 10^k,
        where k depends on the currency code.
        """
        k = CURRENCY_CODE_MAPS.get(currency.name, 2)
        paymentAmount = int(tools.float_round(amount, k) * (10**k))
        return paymentAmount

    def _get_order_detail(self, values):
        name = values["reference"].split("-")[0]
        order = self.env['sale.order'].search([('name', '=', name)])[0]
        details = []
        for line in order.order_line:
            details.append({
                "name": line.display_name,
                "price_total": line.price_total,
                "count" : line.product_uom_qty,
                "currency": order.currency_id.name
            })
        return details


    def ducapi_form_generate_values(self, values):
        self.ensure_one()
        self.pending_msg = ""
        base_url = self.get_base_url()

        headers = {"x-api-key": self.ducapi_api_key}

        data = {"phone": self.ducapi_merchant_phone, "password": self.ducapi_merchant_password}
        url = self.ducapi_api_url + "/auth/login"

        resp = requests.post(url, data, headers=headers)

        self.ducapi_token = json.loads(resp.text)["accessToken"]
        # paymentAmount = self._ducapi_convert_amount(values['amount'], values['currency'])
        headers = {
            "Authorization": "Bearer " + self.ducapi_token,
            "x-api-key": self.ducapi_api_key,
            "Content-Type": "application/json",
        }

        # "%d" % (paymentAmount/100),
        data = {
            "merchantId": values['reference'],
            "externalID": self.env.cr.dbname + '-' + values['reference'],
            "toWalletAddress": self.ducapi_merchant_wallet_address,
            "amount": values["amount"],
            "currency": values['currency'].name,
            "callbackUrl": urls.url_join(base_url, DUCApiController._return_url),
            "expiryTime": {
                "amount": 24,
                "unit": "hours"
            },
            "description": "Pay to Merchant Duc Wallet"
        }

        url = self.ducapi_api_url + self.ducapi_payment_page

        resp = requests.post(url, data=json.dumps(data), headers=headers)

        self.pending_msg = '<div>Open Duc App in your phone and Scan QR code to pay</div><div style="display:inline-flex; padding-top:10px"><a style="display:inline-flex; padding-right:10px" ' \
                           'href="https://play.google.com/store/apps/details?id=com.ducwallet" target="_blank"><img ' \
                           'src="/payment_ducapi/static/src/img/duc-google-play.png" alt="Envio dinero Cuba" ' \
                           'width="150" height="50"></a><a ' \
                           'href="https://apps.apple.com/us/app/duc-app/id1474468151" target="_blank"><img ' \
                           'src="/payment_ducapi/static/src/img/duc-app-store.png" alt="Envio dinero Cuba" ' \
                           'width="150" height="50"></a></div> '

        qrCode = json.loads(resp.text).get("qrCode", "")
        payLink = json.loads(resp.text).get("dynamicLink", "")

        if qrCode != "":
            self.pending_msg = self.pending_msg + _('''<div><img id='qrCode' src='%(qrCode)s' /></div>''') % {
                'qrCode': qrCode,
            }

        if payLink != "":
            self.pending_msg = self.pending_msg +  _('''<div><a href='%(payLink)s'>Pagar con App</a></div>''') % {
                'payLink': payLink,
            }

        values.update({
            "reference": values['reference'],
            # "amount": '%d' % (paymentAmount / 100),
            "currency": values['currency'].name,
            "qrCode": qrCode,
            "status": "pending",
        })

        return values

    def ducapi_get_form_action_url(self):
        return '/payment/ducapi/feedback'


class TxDUCApi(models.Model):
    _inherit = 'payment.transaction'

    @api.model
    def _ducapi_form_get_tx_from_data(self, data):
        reference = data.get('reference')
        tx = self.search([('reference', '=', reference)])

        if not tx or len(tx) > 1:
            error_msg = _('received data for reference %s') % (pprint.pformat(reference))
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        return tx

    def _ducapi_form_get_invalid_parameters(self, data):
        invalid_parameters = []

        if float_compare(float(data.get('amount') or '0.0'), self.amount, 2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % self.amount))
        if data.get('currency') != self.currency_id.name:
            invalid_parameters.append(('currency', data.get('currency'), self.currency_id.name))

        return invalid_parameters

    def _ducapi_form_validate(self, data):
        _logger.info('Validated transfer payment for tx %s: set as pending' % (self.reference))
        if data.get('status') == 'pending':
            self._set_transaction_pending()

        if data.get('status') == 'done':
            self._set_transaction_done()

        return True
